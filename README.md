# About

![iHaze Screenshots](http://i.imgur.com/OELO4Ts.jpg)

This app is developed as a part of Websight Sdn Bhd's technical test.

# Running the App

* Open `iHaze.xcworkspace` and hit `⌘R` to run
* It is advisable to update the app's dependencies by running `pod install` first
