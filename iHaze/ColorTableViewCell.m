//
//  ColorTableViewCell.m
//  iHaze
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "ColorTableViewCell.h"
#import "HexColors.h"

@implementation ColorTableViewCell

- (void)awakeFromNib {
    self.colorView.layer.cornerRadius = 15;
    self.colorView.clipsToBounds = YES;
}

- (void)configureWithColor:(NSDictionary *)color {
    self.colorView.backgroundColor = [UIColor colorWithHexString:[color objectForKey:@"hex"]];
    self.statusLabel.text = [color objectForKey:@"status"];
    self.criteriaLabel.text = [color objectForKey:@"criteria"];
}

@end
