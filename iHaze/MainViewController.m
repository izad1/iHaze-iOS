//
//  MainViewController.m
//  iHaze
//
//  Created by Izad Che Muda on 13/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "MainViewController.h"
#import "Constants.h"
#import "TimelineTableViewCell.h"
#import "TopTimelineTableViewCell.h"
#import "BottomTimelineTableViewCell.h"
#import "CityTableViewCell.h"
#import "LegendsTableViewCell.h"
#import "LeftDropdownView.h"
#import "RightDropdownView.h"
#import "SlidingViewController.h"
#import "NavigationController.h"
#import "CalendarViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "InfoTableViewController.h"

@implementation MainViewController

- (id)initWithCity:(NSDictionary *)city date:(NSDate *)date {
    self = [super init];
    
    if (self) {
        self.city = city;
        self.date = date;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - kNavBarHeight)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"CityTableViewCell" bundle:nil] forCellReuseIdentifier:@"city"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TimelineTableViewCell" bundle:nil] forCellReuseIdentifier:@"timeline"];
    [self.tableView registerNib:[UINib nibWithNibName:@"TopTimelineTableViewCell" bundle:nil] forCellReuseIdentifier:@"topTimeline"];
    [self.tableView registerNib:[UINib nibWithNibName:@"BottomTimelineTableViewCell" bundle:nil] forCellReuseIdentifier:@"bottomTimeline"];
    [self.tableView registerNib:[UINib nibWithNibName:@"LegendsTableViewCell" bundle:nil] forCellReuseIdentifier:@"legends"];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight / 3)];
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, headerView.frame.size.height)];
    [headerView addSubview:self.mapView];
    [self setupMapView];
    
    
    [self.tableView addParallaxWithView:headerView andHeight:headerView.frame.size.height];
    self.tableView.parallaxView.shadowView.hidden = YES;
    
    CGFloat height = 30;
    
    LeftDropdownView *leftDropdownView = [[[NSBundle mainBundle] loadNibNamed:@"LeftDropdownView" owner:self options:nil] objectAtIndex:0];
    leftDropdownView.frame = CGRectMake(0, 0, (kScreenWidth / 3) * 2, height);
    [leftDropdownView configureWithCity:self.city];
    [leftDropdownView.button addTarget:self action:@selector(leftDropdownTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftDropdownView];


    RightDropdownView *rightDropdownView = [[[NSBundle mainBundle] loadNibNamed:@"RightDropdownView" owner:self options:nil] objectAtIndex:0];
    rightDropdownView.frame = CGRectMake(0, 0, kScreenWidth / 3, height);
    [rightDropdownView configureWithDate:self.date];
    [rightDropdownView.button addTarget:self action:@selector(rightDropdownTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightDropdownView];
    

    [self.view addSubview:self.tableView];
    self.originalContentOffset = self.tableView.contentOffset;
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    if (indexPath) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return 24;
    }
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"city" forIndexPath:indexPath];
        [cell configureWithCity:self.city];
        
        return cell;

    }
    else if (indexPath.section == 1) {
        TimelineTableViewCell *cell;
        
        if (indexPath.row == 0) {
            cell = (TopTimelineTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"topTimeline" forIndexPath:indexPath];            
        }
        else if (indexPath.row == 23) {
            cell = (BottomTimelineTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bottomTimeline" forIndexPath:indexPath];
        }
        else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"timeline" forIndexPath:indexPath];
        }
        
        [cell configureWithCity:self.city indexPath:indexPath];
        
        return cell;
    }
    else if (indexPath.section == 2) {
        LegendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"legends" forIndexPath:indexPath];
        [cell.button addTarget:self action:@selector(infoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

        [cell.button setHighlightBlock:^(ProxyButton *button) {
            cell.buttonContainerView.backgroundColor = [UIColor colorWithHexString:@"#EFF9FC"];
        } dehighlightBlock:^(ProxyButton *button) {
            cell.buttonContainerView.backgroundColor = [UIColor whiteColor];
        }];
        
        return cell;
    }
    
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return 160;
            
        case 1:
            if (indexPath.row == 0) {
                return 71;
            }
            else if (indexPath.row == 23) {
                return 111;
            }
            
            return 90;
            
        case 2:
            return 100;
            
        default:
            return 0;
    }
}


- (void)leftDropdownTapped:(ProxyButton *)sender {
    CitiesTableViewController *citiesTVC = [CitiesTableViewController new];
    citiesTVC.delegate = self;
    
    NavigationController *nc = [[NavigationController alloc] initWithRootViewController:citiesTVC];
    
    SlidingViewController *slidingVC = [[SlidingViewController alloc] initWithViewController:nc];
    [self presentViewController:slidingVC animated:NO completion:nil];
}


- (void)rightDropdownTapped:(ProxyButton *)sender {
    CalendarViewController *calendarVC = [CalendarViewController new];
    calendarVC.delegate = self;
    calendarVC.date = self.date;
    
    NavigationController *nc = [[NavigationController alloc] initWithRootViewController:calendarVC];
    
    SlidingViewController *slidingVC = [[SlidingViewController alloc] initWithViewController:nc];
    [self presentViewController:slidingVC animated:NO completion:nil];
}


- (void)didSelectCity:(NSDictionary *)city {
    self.city = city;
    [self.tableView reloadData];
    [self.tableView setContentOffset:self.originalContentOffset animated:YES];
    
    LeftDropdownView *leftDropdownView = self.navigationItem.leftBarButtonItem.customView;
    [leftDropdownView configureWithCity:self.city];
    
    [self setupMapView];
}


- (void)didSelectDate:(NSDate *)date {
    NSDate *previousDate = self.date;
    self.date = date;
    
    RightDropdownView *rightDropdownView = self.navigationItem.rightBarButtonItem.customView;
    [rightDropdownView configureWithDate:self.date];

    NSInteger index = [appDelegate.cities indexOfObject:self.city];
        
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [appDelegate fetchCitiesWithDate:self.date completion:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                self.city = [appDelegate.cities objectAtIndex:index];
                [self.tableView reloadData];
                
                [self.tableView setContentOffset:self.originalContentOffset animated:YES];
            });
        } failure:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                self.date = previousDate;
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                         message:@"There is a problem fetching data from the Department of Environment."
                                                                                  preferredStyle:UIAlertControllerStyleAlert];
                
                [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * _Nonnull action) {
                                                                      [rightDropdownView configureWithDate:self.date];
                                                                  }]];
                
                [self presentViewController:alertController animated:YES completion:nil];
            });
        }];
    });
}


- (void)setupMapView {
    [self.mapView removeAnnotations:self.mapView.annotations];

    NSNumber *lat = [self.city objectForKey:@"lat"];
    NSNumber *lng = [self.city objectForKey:@"lng"];
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(lat.floatValue, lng.floatValue);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.05, 0.05);
    
    MKPointAnnotation *annotation = [MKPointAnnotation new];
    annotation.coordinate = coord;
    [self.mapView addAnnotation:annotation];
    
    [self.mapView setRegion:MKCoordinateRegionMake(coord, span) animated:YES];
}


- (void)infoButtonTapped:(ProxyButton *)sender {    
    InfoTableViewController *infoTVC = [InfoTableViewController new];
    NavigationController *nc = [[NavigationController alloc] initWithRootViewController:infoTVC];
    
    SlidingViewController *slidingVC = [[SlidingViewController alloc] initWithViewController:nc];
    [self presentViewController:slidingVC animated:NO completion:nil];
}

@end
