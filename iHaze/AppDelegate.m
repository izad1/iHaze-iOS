//
//  AppDelegate.m
//  iHaze
//
//  Created by Izad Che Muda on 13/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "LoadingViewController.h"


@implementation AppDelegate

AppDelegate *appDelegate = nil;


- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = [LoadingViewController new];
    
    return YES;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    appDelegate = self;
    
    self.window.tintColor = kTintColor;
    [self.window makeKeyAndVisible];
    
    self.cities = [[NSMutableArray alloc] init];
    
    return YES;
}


- (void)fetchCitiesWithDate:(NSDate *)date completion:(void (^)())completion failure:(void (^)())failure {
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    
    [manager GET:[NSString stringWithFormat:@"%@/city", kBaseURL]
      parameters:@{ @"date": [dateFormat stringFromDate:date] }
         success:^(AFHTTPRequestOperation *operation, NSArray *cities) {
             if (cities.count) {
                 [self.cities removeAllObjects];
                 
                 [cities enumerateObjectsUsingBlock:^(NSDictionary *city, NSUInteger idx, BOOL *stop) {
                     [self.cities addObject:city];
                 }];
                 
                 completion();
             }
             else {
                 failure();
             }                                                                     
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             failure();
             NSLog(@"%@", error);
         }];
}

@end
