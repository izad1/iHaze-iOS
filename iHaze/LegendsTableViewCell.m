//
//  LegendsTableViewCell.m
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "LegendsTableViewCell.h"
#import "Constants.h"

@implementation LegendsTableViewCell

- (void)awakeFromNib {
    self.buttonContainerView.layer.borderColor = kTintColor.CGColor;
    self.buttonContainerView.layer.borderWidth = 1;
    self.buttonContainerView.layer.cornerRadius = 20;
}

@end
