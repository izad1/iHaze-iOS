//
//  ProxyButton.h
//  iMudah
//
//  Created by Izad Che Muda on 10/16/15.
//  Copyright © 2015 Elite Mobile Global Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ProxyButtonBlock)(id);

@interface ProxyButton : UIButton

@property (nonatomic, copy) ProxyButtonBlock highlightBlock;
@property (nonatomic, copy) ProxyButtonBlock dehighlightBlock;


- (void)setHighlightBlock:(ProxyButtonBlock)highlightBlock dehighlightBlock:(ProxyButtonBlock)dehighlightBlock;

@end
