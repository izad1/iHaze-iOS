//
//  ColorTableViewCell.h
//  iHaze
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *criteriaLabel;

- (void)configureWithColor:(NSDictionary *)color;

@end
