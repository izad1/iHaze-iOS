//
//  CityTableViewCell.h
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

- (void)configureWithCity:(NSDictionary *)city;

@end
