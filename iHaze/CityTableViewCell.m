//
//  CityTableViewCell.m
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "CityTableViewCell.h"

@implementation CityTableViewCell

- (void)configureWithCity:(NSDictionary *)city {
    self.nameLabel.text = [city objectForKey:@"name"];
    self.stateLabel.text = [city objectForKey:@"state"];
}

@end
