//
//  TimelineTableViewCell.m
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "TimelineTableViewCell.h"
#import "HexColors.h"
#import "Constants.h"

@implementation TimelineTableViewCell

- (void)configureWithCity:(NSDictionary *)city indexPath:(NSIndexPath *)indexPath {
    NSArray *hours = @[
        @"12:00AM",
        @"01:00AM",
        @"02:00AM",
        @"03:00AM",
        @"04:00AM",
        @"05:00AM",
        @"06:00AM",
        @"07:00AM",
        @"08:00AM",
        @"09:00AM",
        @"10:00AM",
        @"11:00AM",
        @"12:00PM",
        @"01:00PM",
        @"02:00PM",
        @"03:00PM",
        @"04:00PM",
        @"05:00PM",
        @"06:00PM",
        @"07:00PM",
        @"08:00PM",
        @"09:00PM",
        @"10:00PM",
        @"11:00PM"
    ];
    
    NSDictionary *readings = [city objectForKey:@"readings"];
    
    id reading = [readings objectForKey:[hours objectAtIndex:indexPath.row]];
    
    if ([reading isKindOfClass:[NSString class]]) {
        NSString *stringVal = (NSString *)reading;
        self.readingLabel.text = stringVal;
        
        NSInteger intVal = stringVal.integerValue;
        
        if (intVal <= 50) {
            self.readingLabel.textColor = [UIColor colorWithHexString:@"#0000FF"];
        }
        else if (intVal >= 51 && intVal <= 100) {
            self.readingLabel.textColor = [UIColor colorWithHexString:@"#008000"];
        }
        else if (intVal >= 101 && intVal <= 200) {
            self.readingLabel.textColor = [UIColor colorWithHexString:@"#ffff00"];
        }
        else if (intVal >= 201 && intVal <= 300) {
            self.readingLabel.textColor = [UIColor colorWithHexString:@"#f57825"];
        }
        else if (intVal > 300) {
            self.readingLabel.textColor = [UIColor colorWithHexString:@"#cc0000"];
        }
    }
    else {
        self.readingLabel.text = @"n/a";
        self.readingLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    }
    
    NSString *time = [hours objectAtIndex:indexPath.row];
    self.timeLabel.text = [time substringWithRange:NSMakeRange(0, 5)];
    self.periodLabel.text = [time substringFromIndex:5];
}

@end
