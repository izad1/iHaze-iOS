//
//  SlidingViewController.m
//  iMudah
//
//  Created by Izad Che Muda on 10/19/15.
//  Copyright © 2015 Elite Mobile Global Sdn Bhd. All rights reserved.
//

#import "SlidingViewController.h"
#import "Constants.h"


@implementation SlidingViewController

- (id)initWithViewController:(UIViewController *)viewController {
    self = [super init];
    
    if (self) {
        self.viewController = viewController;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Background view
    
    self.backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    self.backgroundView.backgroundColor = [UIColor clearColor];
    self.backgroundView.alpha = 0;
    [self.backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(close:)]];
    
    UIVisualEffectView  *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    visualEffectView.frame = self.backgroundView.frame;
    [self.backgroundView addSubview:visualEffectView];
    
    
    // Content view
    
    self.originalContentViewFrame = CGRectMake(20, (kNavBarHeight + 1) - kScreenHeight, kScreenWidth - 40, kScreenHeight - (kNavBarHeight + 1) - kNavBarHeight);
    self.contentView = [[UIView alloc] initWithFrame:self.originalContentViewFrame];
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    
    // Add all subviews
    
    [self.view addSubview:self.backgroundView];
    [self.view addSubview:self.contentView];
    
    
    // VC stuff
    
    [self addChildViewController:self.viewController];
    
    self.viewController.view.frame = self.contentView.bounds;
    [self.contentView addSubview:self.viewController.view];
    
    [self.viewController didMoveToParentViewController:self];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.backgroundView.alpha = 1;
                         self.contentView.frame = CGRectMake(self.originalContentViewFrame.origin.x, kNavBarHeight + 1, self.originalContentViewFrame.size.width, self.originalContentViewFrame.size.height);
                     } completion:^(BOOL finished) {
                         
                     }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (UIModalPresentationStyle)modalPresentationStyle {
    return  UIModalPresentationCustom;
}


- (void)close:(id)sender {
    [self dismissWithCompletion:nil];
}

- (void)dismissWithCompletion:(void (^)())completion {
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundView.alpha = 0;
        self.contentView.frame = self.originalContentViewFrame;
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:completion];
    }];
}

@end
