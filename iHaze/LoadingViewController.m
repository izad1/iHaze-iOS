//
//  LoadingViewController.m
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "LoadingViewController.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "NavigationController.h"
#import "MainViewController.h"
#import "NSDate+Calendar.h"


@implementation LoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = kTintColor;
    
    self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.indicatorView.center = self.view.center;
    [self.indicatorView startAnimating];
    [self.view addSubview:self.indicatorView];
    
    self.label = [UILabel new];
    self.label.text = @"Fetching data";
    [self.label sizeToFit];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.center = CGPointMake(self.view.center.x, self.view.center.y + self.indicatorView.frame.size.height + 4);
    self.label.font = [UIFont systemFontOfSize:12];
    self.label.textColor = [UIColor whiteColor];
    [self.view addSubview:self.label];
    
    self.button = [UIButton new];
    [self.button setTitle:@"Tap to refresh" forState:UIControlStateNormal];
    self.button.titleLabel.font = [UIFont systemFontOfSize:14];
    self.button.tintColor = [UIColor whiteColor];
    [self.button sizeToFit];
    self.button.center = self.view.center;
    self.button.hidden = YES;
    [self.button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.button];
    
    [self fetchCities];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)fetchCities {
    self.indicatorView.hidden = NO;
    self.label.hidden = NO;
    self.button.hidden = YES;
    
    NSDate *date;
    NSDate *today = [NSDate date];
    
    if (today.hour < 7) {
        date = [today dateYesterday];
    }
    else {
        date = today;
    }
    
    [appDelegate fetchCitiesWithDate:date completion:^{        
        MainViewController *mainVC = [[MainViewController alloc] initWithCity:[appDelegate.cities lastObject]
                                                                         date:date];
        
        NavigationController *nc = [[NavigationController alloc] initWithRootViewController:mainVC];
        
        [self presentViewController:nc animated:YES completion:nil];
    } failure:^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                 message:@"There is a problem fetching data from the Department of Environment."
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              self.indicatorView.hidden = YES;
                                                              self.label.hidden = YES;
                                                              self.button.hidden = NO;
                                                          }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }];
}


- (void)buttonTapped:(UIButton *)sender {
    [self fetchCities];
}

@end
