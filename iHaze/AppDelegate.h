//
//  AppDelegate.h
//  iHaze
//
//  Created by Izad Che Muda on 13/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

extern AppDelegate *appDelegate;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray *cities;

- (void)fetchCitiesWithDate:(NSDate *)date completion:(void (^)())completion failure:(void (^)())failure;

@end

