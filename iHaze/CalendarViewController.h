//
//  CalendarViewController.h
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JTCalendar/JTCalendar.h>
#import "CalendarDelegate.h"

@interface CalendarViewController : UIViewController <JTCalendarDelegate>

@property (nonatomic, strong) JTCalendarManager *manager;
@property (nonatomic, strong) JTCalendarMenuView *menuView;
@property (nonatomic, strong) JTHorizontalCalendarView *contentView;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, assign) id <CalendarDelegate> delegate;

- (void)closeButtonTapped:(UIBarButtonItem *)sender;
- (BOOL)dateIsWithinAWeek:(NSDate *)date;

@end
