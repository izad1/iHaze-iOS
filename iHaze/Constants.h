//
//  Constants.h
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HexColors.h"

#define kAppName @"iHaze"
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kNavBarHeight 64
#define kBaseURL @"http://ihaze.babyllon.com"
#define kTintColor [UIColor colorWithHexString:@"#27A2CF"]