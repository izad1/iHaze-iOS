//
//  InfoTableViewController.h
//  iHaze
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *colors;
@property (nonatomic, strong) NSArray *symbols;

- (void)closeButtonTapped:(UIBarButtonItem *)sender;

@end
