//
//  CitiesTableViewController.h
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CitiesDelegate.h"

@interface CitiesTableViewController : UITableViewController

@property (nonatomic, assign) id <CitiesDelegate> delegate;

- (void)closeButtonTapped:(UIBarButtonItem *)sender;

@end
