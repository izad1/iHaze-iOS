//
//  SymbolTableViewCell.m
//  iHaze
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "SymbolTableViewCell.h"

@implementation SymbolTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)configureWithSysmbol:(NSDictionary *)symbol {
    self.characterLabel.text = [symbol objectForKey:@"character"];
    self.descriptionLabel.text = [symbol objectForKey:@"description"];
}

@end
