//
//  SlidingViewController.h
//  iMudah
//
//  Created by Izad Che Muda on 10/19/15.
//  Copyright © 2015 Elite Mobile Global Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlidingViewController : UIViewController

- (id)initWithViewController:(UIViewController *)viewController;

@property (nonatomic, strong) UIViewController *viewController;
@property (nonatomic) CGRect originalContentViewFrame;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *backgroundView;

- (void)close:(id)sender;
- (void)dismissWithCompletion:(void (^)())completion;

@end
