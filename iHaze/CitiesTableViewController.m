//
//  CitiesTableViewController.m
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "CitiesTableViewController.h"
#import "AppDelegate.h"
#import "HexColors.h"
#import "SlidingViewController.h"


@implementation CitiesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Select a City";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(closeButtonTapped:)];

    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return appDelegate.cities.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *city = [appDelegate.cities objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = [city objectForKey:@"name"];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"#444444"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}



#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate didSelectCity:[appDelegate.cities objectAtIndex:indexPath.row]];
    
    SlidingViewController *slidingVC = (SlidingViewController *)self.parentViewController.parentViewController;
    [slidingVC dismissWithCompletion:nil];
}



#pragma mark - Misc.


- (void)closeButtonTapped:(UIBarButtonItem *)sender {
    SlidingViewController *slidingVC = (SlidingViewController *)self.parentViewController.parentViewController;
    [slidingVC dismissWithCompletion:nil];
}

@end
