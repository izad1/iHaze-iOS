//
//  LoadingViewController.h
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingViewController : UIViewController

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIButton *button;

- (void)fetchCities;
- (void)buttonTapped:(UIButton *)sender;

@end
