//
//  RightDropdownView.m
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "RightDropdownView.h"

@implementation RightDropdownView

- (void)configureWithDate:(NSDate *)date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMMM dd"];
    
    self.textLabel.text = [dateFormat stringFromDate:date];
}

@end
