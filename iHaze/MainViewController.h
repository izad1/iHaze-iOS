//
//  MainViewController.h
//  iHaze
//
//  Created by Izad Che Muda on 13/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "UIScrollView+APParallaxHeader.h"
#import "ProxyButton.h"
#import "CitiesTableViewController.h"
#import "CalendarDelegate.h"


@interface MainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CitiesDelegate, CalendarDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSDictionary *city;
@property (nonatomic) CGPoint originalContentOffset;

- (id)initWithCity:(NSDictionary *)city date:(NSDate *)date;
- (void)leftDropdownTapped:(ProxyButton *)sender;
- (void)rightDropdownTapped:(ProxyButton *)sender;
- (void)setupMapView;
- (void)infoButtonTapped:(ProxyButton *)sender;

@end

