//
//  InfoTableViewController.m
//  iHaze
//
//  Created by Izad Che Muda on 15/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "InfoTableViewController.h"
#import "SlidingViewController.h"
#import "ColorTableViewCell.h"
#import "SymbolTableViewCell.h"


@implementation InfoTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"API Color Reference";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(closeButtonTapped:)];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.tableView.frame style:UITableViewStyleGrouped];
    [self.tableView registerNib:[UINib nibWithNibName:@"ColorTableViewCell" bundle:nil] forCellReuseIdentifier:@"color"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SymbolTableViewCell" bundle:nil] forCellReuseIdentifier:@"symbol"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return self.colors.count;
        
        case 1:
            return self.symbols.count;
            
        default:
            return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ColorTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"color" forIndexPath:indexPath];
        [cell configureWithColor:[self.colors objectAtIndex:indexPath.row]];
        
        return cell;
    }
    else if (indexPath.section == 1) {
        SymbolTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"symbol" forIndexPath:indexPath];
        [cell configureWithSysmbol:[self.symbols objectAtIndex:indexPath.row]];
        
        return cell;
    }
    
    return nil;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"Color Reference";
            
        case 1:
            return @"Symbol Reference";
            
        default:
            return nil;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}



#pragma mark - Misc.


- (void)closeButtonTapped:(UIBarButtonItem *)sender {
    SlidingViewController *slidingVC = (SlidingViewController *)self.parentViewController.parentViewController;
    [slidingVC dismissWithCompletion:nil];
}


- (NSArray *)colors {
    return @[
        @{
            @"hex": @"#0000FF",
            @"status": @"Good",
            @"criteria": @"Below 50"
        },
        @{
            @"hex": @"#008000",
            @"status": @"Moderate",
            @"criteria": @"51 - 100"
        },
        @{
            @"hex": @"#ffff00",
            @"status": @"Unhealthy",
            @"criteria": @"101 - 200"
        },
        @{
            @"hex": @"#f57825",
            @"status": @"Very Unhealthy",
            @"criteria": @"201 - 300"
        },
        @{
            @"hex": @"#cc0000",
            @"status": @"Hazardous",
            @"criteria": @"More than 300"
        }
    ];
}


- (NSArray *)symbols {
    return @[
        @{
            @"character": @"*",
            @"description": @"Particulate matter with diameter of less than 10 micron (PM10)"
        },
        @{
            @"character": @"a",
            @"description": @"Sulphur Dioxide (SO2)"
        },
        @{
            @"character": @"b",
            @"description": @"Nitrogen Dioxide (NO2)"
        },
        @{
            @"character": @"c",
            @"description": @"Ozone (O3)"
        },
        @{
            @"character": @"d",
            @"description": @"Carbon Monoxide (CO)"
        },
        @{
            @"character": @"&",
            @"description": @"More than one pollutant"
        }
    ];
}

@end
