//
//  LeftDropdownView.m
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "LeftDropdownView.h"

@implementation LeftDropdownView

- (void)configureWithCity:(NSDictionary *)city {
    NSString *name = [city objectForKey:@"name"];
    NSArray *comps = [name componentsSeparatedByString:@","];
    self.textLabel.text = [comps objectAtIndex:0];
}

@end
