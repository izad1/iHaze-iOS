//
//  CitiesDelegate.h
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CitiesDelegate <NSObject>

- (void)didSelectCity:(NSDictionary *)city;

@end