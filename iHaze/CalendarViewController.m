//
//  CalendarViewController.m
//  iHaze
//
//  Created by Izad Che Muda on 14/11/2015.
//  Copyright © 2015 Izad Che Muda. All rights reserved.
//

#import "CalendarViewController.h"
#import "Constants.h"
#import "SlidingViewController.h"
#import "NSDate+Calendar.h"


@implementation CalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Select a Date";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(closeButtonTapped:)];
    
    self.view.frame = self.navigationController.view.frame;
    
    self.menuView = [[JTCalendarMenuView alloc] initWithFrame:CGRectMake(20, 0, self.view.frame.size.width - 40, 50)];
    self.contentView = [[JTHorizontalCalendarView alloc] initWithFrame:CGRectMake(20, self.menuView.frame.size.height, self.view.frame.size.width - 40, self.view.frame.size.height - self.menuView.frame.size.height)];
    
    [self.view addSubview:self.menuView];
    [self.view addSubview:self.contentView];
    
    self.manager = [[JTCalendarManager alloc] init];
    self.manager.delegate = self;
    self.manager.menuView = self.menuView;
    self.manager.contentView = self.contentView;
    self.manager.date = self.date;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView {
    dayView.circleView.hidden = YES;
    dayView.textLabel.textColor = [UIColor colorWithHexString:@"#dedede"];
    
    if ([calendar.dateHelper date:self.date isTheSameDayThan:dayView.date]) {
        // Today
        
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = kTintColor;
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    else if ([self dateIsWithinAWeek:dayView.date]) {
        dayView.textLabel.textColor = [UIColor colorWithHexString:@"#444444"];
    }
}


- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView {
    if ([self dateIsWithinAWeek:dayView.date]) {
        SlidingViewController *slidingVC = (SlidingViewController *)self.parentViewController.parentViewController;
        
        [slidingVC dismissWithCompletion:^{
            [self.delegate didSelectDate:dayView.date];
        }];
    }
}


- (void)closeButtonTapped:(UIBarButtonItem *)sender {
    SlidingViewController *slidingVC = (SlidingViewController *)self.parentViewController.parentViewController;
    [slidingVC dismissWithCompletion:nil];
}


- (BOOL)dateIsWithinAWeek:(NSDate *)date {
    NSDate *today = [NSDate date];
    
    if ([self.manager.dateHelper date:today isTheSameDayThan:date]) {
        return YES;
    }
    
    for (int i=1; i<=6; i++) {
        if ([self.manager.dateHelper date:[today dateBySettingDay:today.day - i] isTheSameDayThan:date]) {
            return YES;
        }
    }

    return NO;
}

@end
